import subprocess

def get_git_diff(node1, node2, filename, git_dir, work_tree):
    cmd = [
        'git', '--git-dir', git_dir, '--work-tree', work_tree,
        'diff', node1 + "..." + node2, '--', filename
    ]
    result = subprocess.run(cmd, capture_output=True, text=True)
    return result.stdout

def get_git_blame(line_num, filename, git_dir, work_tree):
    cmd = [
        'git', '--git-dir', git_dir, '--work-tree', work_tree,
        'blame', '-L', f"{line_num},{line_num}", '-p', '--', filename
    ]
    result = subprocess.run(cmd, capture_output=True, text=True)
    commit_message = None
    for line in result.stdout.split("\n"):
        if line.startswith("summary "):
            commit_message = line.split("summary ", 1)[1]
            break
    return commit_message, result.stdout

def get_changes_for_commit(commit_id, filename, git_dir, work_tree):
    cmd = [
        'git', '--git-dir', git_dir, '--work-tree', work_tree,
        'show', commit_id, '--', filename
    ]
    result = subprocess.run(cmd, capture_output=True, text=True)
    return result.stdout

def get_commits_for_file(node1, node2, filename, git_dir, work_tree):
    cmd = [
        'git', '--git-dir', git_dir, '--work-tree', work_tree,
        'log', '--format=%H', f"{node1}...{node2}", '--', filename
    ]
    result = subprocess.run(cmd, capture_output=True, text=True)
    return result.stdout.splitlines()

def find_commit_for_removed_line(line, commits, filename, git_dir, work_tree):
    for commit in commits:
        changes = get_changes_for_commit(commit, filename, git_dir, work_tree)
        if line in changes:
            return commit
    return None

def get_commit_details(commit_id, git_dir, work_tree):
    cmd = [
        'git', '--git-dir', git_dir, '--work-tree', work_tree,
        'show', '-s', '--format=%H %s %an', commit_id
    ]
    result = subprocess.run(cmd, capture_output=True, text=True)
    parts = result.stdout.strip().split(' ', 2)
    if len(parts) == 3:
        return parts[0], parts[1], parts[2]
    return None, None, None

def get_message_and_author_by_node_id(node_id, git_dir, work_tree):
    cmd = [
        'git', '--git-dir', git_dir, '--work-tree', work_tree,
        'show', '-s', '--format=%s %an', node_id
    ]
    result = subprocess.run(cmd, capture_output=True, text=True)
    parts = result.stdout.strip().split(' ', 1)
    if len(parts) == 2:
        return parts[0], parts[1]
    return None, None

def main():
    node1 = "85f81eb9c1ec53f81050e8d0752277cc0ffb526f"
    node2 = "9cd5b49d31130c9a599fb6f92a589f20ea1a2485"
    filename = "E:\\Git\\ZK\\zk-label.txt"
    git_dir = "E:\\Git\\ZK\\.git"
    work_tree = "E:\\Git\\ZK"

    diff = get_git_diff(node1, node2, filename, git_dir, work_tree)
    diff_lines = diff.split("\n")
    flag = False
    actual_line_num = 0
    del_num = 0
    for i, line in enumerate(diff_lines):
        if flag is False and not line.startswith('@@'):
            continue
        if line.startswith('@@'):
            actual_line_num = int(line.split()[2].split(',')[0])
            del_num = int(line.split()[1].split(',')[0].replace('-', ''))
            flag = True
            continue
        if line.startswith('-'):
            message, blame_info = get_git_blame(actual_line_num, filename, git_dir, work_tree)
            parts = blame_info.split("\n")
            if len(parts) >= 2:
                node_id = (parts[0])[0:8]
                author = parts[1].replace("author", "").strip()
                changes = get_changes_for_commit(node_id, filename, git_dir, work_tree)
                if line not in changes:
                    commits = get_commits_for_file(node1, node2, filename, git_dir, work_tree)
                    correct_commit = find_commit_for_removed_line(line, commits, filename, git_dir, work_tree)
                    if correct_commit:
                        # Get commit details for correct_commit and print
                        # ...
                        message,author = get_message_and_author_by_node_id(correct_commit, git_dir, work_tree)
                        correct_commit = correct_commit[0:8]
                        print(f"[{correct_commit}:{message}:{author}:{del_num}] {line}")
                    else:
                        print("Not Match")
                else:
                    print(f"[{node_id}:{message}:{author}:{del_num}] {line}")
            else:
                print(line)
        elif line.startswith('+'):
            message, blame_info = get_git_blame(actual_line_num, filename, git_dir, work_tree)
            parts = blame_info.split("\n")
            if len(parts) >= 2:
                node_id = (parts[0])[0:8]
                author = parts[1].replace("author", "").strip()
                print(f"[{node_id}:{message}:{author}:{actual_line_num}] {line}")
            else:
                print(line)
        else:
            print(line)
        if not line.startswith('-'):
            actual_line_num += 1
        del_num += 1

if __name__ == "__main__":
    main()
